import React from 'react';
import App from './src/app';

const RootApp = () => {
  return <App />;
};

export default RootApp;
