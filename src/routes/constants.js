const defaultOptions = {
  headerShown: false,
};
const RoutesName = {
  SPLASH_STACK_SCREEN: 'SplashStack',
  AUTH_STACK_SCREEN: 'AuthStack',
  DASHBOARD_STACK_SCREEN: 'DashboardStack',
  SPLASH_SCREEN: 'SplashScreen',
  LOGIN_SCREEN: 'LoginScreen',
  SIGNUP_SCREEN: 'SignupScreen',
  HOME_STACK_SCREEN: 'HomeStackScreen',
  HOME_SCREEN: 'HomeScreen',
  PROFILE_SCREEN: 'ProfileScreen',
  INTERPRETATION_SCREEN: 'InterPretationScreen',
  TRANSPORTATION_SCREEN: 'TransportationScreen',
  DELIVERY_SCREEN: 'DeliveryScreen',
  PAYOUT_SCREEN: 'PayoutScreen',
  CMR_STACK_SCREEN: 'CmrStackScreen',
  CMR_SCREEN: 'CmrScreen',
  CMR_PENDING_SCREEN: 'CmrPendingScreen',
  CMR_PENDING_STACK_SCREEN: 'CmrPendingStackScreen',
  CMR_CONFIRMED_SCREEN: 'CmrConfirmedScreen',
  CMR_COMPLETED_SCREEN: 'CmrCompletedScreen',
  CMR_DETAILS_SCREEN: 'CmrDetailScreen',
};
export {defaultOptions, RoutesName};
