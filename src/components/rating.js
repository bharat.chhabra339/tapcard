import React, {useState} from 'react';
import {ImageComponent} from './index';

const CustomRatingBar = () => {
  const [defaultRating] = useState(5);
  // To set the max number of Stars
  const [maxRating] = useState([1, 2, 3, 4, 5]);
  return (
    <>
      {maxRating.map(item => {
        return (
          <ImageComponent
            name={item <= defaultRating ? 'filled_star' : 'corner_star'}
            height={30}
            width={30}
          />
        );
      })}
    </>
  );
};

export default CustomRatingBar;
